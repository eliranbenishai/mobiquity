Greetings and welcome to my flavor of the F1 Champion App.

One of the most important lessons I've learned over the years is the importance of matching the best
tool to the task at hand. For this reason and others I've decided to write the app in vanilla JS.
It's small, simple and any framework would've created a substantial overhead which would've hindered
performance and development time. You'll notice that I've used some React techniques as those had
proved to be effective, manageable and able to scale well - vastly superior to Angular. I'd be happy
to discuss my conclusions and methods in greater detail.

The app is also responsive and scales well across a multitude of resolutions. I've used several
techniques to achieve this, some of which require newer browsers. There are fallbacks or polyfills
to most of those techniques yet as no browser limitation was mentioned, and I wanted to keep the
development time under 3 hours, those were left out. With that being said, other features were also
left out - like a timed slideshow of the background images, better user-abuse prevention and other
small dabs of polish.

Thank you for your consideration!