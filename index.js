/*
Greetings and welcome to my flavor of the F1 Champion App.

One of the most important lessons I've learned over the years is the importance of matching the best
tool to the task at hand. For this reason and others I've decided to write the app in vanilla JS.
It's small, simple and any framework would've created a substantial overhead which would've hindered
performance and development time. You'll notice that I've used some React techniques as those had
proved to be effective, manageable and able to scale well - vastly superior to Angular. I'd be happy
to discuss my conclusions and methods in greater detail.

The app is also responsive and scales well across a multitude of resolutions. I've used several
techniques to achieve this, some of which require newer browsers. There are fallbacks or polyfills
to most of those techniques yet as no browser limitation was mentioned, and I wanted to keep the
development time under 3 hours, those were left out. With that being said, other features were also
left out - like a timed slideshow of the background images, better user-abuse prevention and other
small dabs of polish.

Thank you for your consideration!
*/

(function(w, d){
	// "constants"
	var C = {
		ANIMATIONDELAY: 50,
		APIPREFIX: 'https://ergast.com/api/f1',
		APISUFFIX: '.json',
		APPCLASS: '.mobiquity',
		DATASPAN: [2005, 2015]
	}

	// Data object - "store"
	var D = {
		seasonList: [],
		winnerList: {
			[C.DATASPAN[1]]: []
		}
	}

	// Reusable snippets - T for Tools
	var T = {
		// The cascade animation used to hide/reveal the Season/Winner List - a callback is attached (and removed) to the last item's 'transitionend' event
		animateList: function(selector, reveal, callback){
			var $winnerListItems = T.qs(selector, true),
				cb = function(ref){
					ref.removeEventListener('transitionend', cb)
					callback && callback();
				};
			for(var index = 0; index < $winnerListItems.length; index++){
				setTimeout(function(item, index){
					if(index === $winnerListItems.length - 1){
						item.addEventListener('transitionend', cb.bind(this, item))
					}
					item.classList[reveal ? 'add' : 'remove']('on');
				}.bind(this, $winnerListItems[index], index), C.ANIMATIONDELAY * index);
			}
		},
		apiConstruct: function(url){
			return C.APIPREFIX + url + C.APISUFFIX;
		},
		// Wrapper for the native 'fetch' method to parse the response into JSON
		fetchJson: function(url, callback){
			fetch(T.apiConstruct(url)).then(function(response){
				response.json().then(function(json){
					if(callback instanceof Function){
						callback(json);
					} else {
						console.warning('No callback function passed for \'fetchJson\'');
					}
				});
			});
		},
		// A querySelector/querySelectorAll wrapper
		qs: function(selector, all){
			return d[all ? 'querySelectorAll' : 'querySelector'](C.APPCLASS + (selector ? ' ' + selector : ''));
		}
	}

	// An event delegator for the Season List - disables the Season List to prevent redundant calls and retrieves the Winner List if needed. Previously fetched lists are cached and aren't fetched again.
	function handleSeasonList(evt){
		var $seasonList = T.qs('.season-list'),
			$seasonListItems = T.qs('.season-list li', true),
			$target = evt.target,
			season = $target.getAttribute('data-season'),
			populateAndReveal = function(){
				render({
					dataNode: D.winnerList[season],
					target: '.winner-list'
				});
				T.animateList('.winner-list li', true, function(){
					$seasonList.classList.remove('disabled');
				});
			};

		for(var index = 0; index < $seasonListItems.length; index++){
			$seasonListItems[index].classList.remove('selected');
		}
		$target.classList.add('selected');

		$seasonList.classList.add('disabled');
		T.animateList('.winner-list li', false, function(){
			if(D.winnerList[season]){
				populateAndReveal();
			} else {
				populateWinnerList(season, function(){
					populateAndReveal();
				});
			}
		});

	}

	// custom Fetch function to parse through the proprietary data structure
	// The eventual custom list of drivers is sorted and each driver's points are aggregated to account for his/her World Championship
	function populateWinnerList(season, callback){
		D.winnerList[season] = [];
		T.fetchJson(`/${season}/results/1`, function(response){
			var curChampion = {code: '', points: 0};
			response.MRData.RaceTable.Races.map(function(item, index){
				var driver = item.Results[0].Driver,
					currentDriver = D.winnerList[season].find(function(driverItem){return driver.code === driverItem.code});

				if(currentDriver){
					currentDriver.points += +item.Results[0].points;
					if(currentDriver.points > curChampion.points){
						curChampion = {code: currentDriver.code, points: currentDriver.points}
					}
				} else {
					D.winnerList[season].push({
						code: driver.code,
						dob: driver.dateOfBirth,
						name: driver.givenName + ' ' + driver.familyName,
						nationality: driver.nationality,
						points: +item.Results[0].points,
						url: driver.url,
						worldChampion: false
					});
				}
			})

			D.winnerList[season].sort((a, b) => {
				if(a.code === curChampion.code){
					a.worldChampion = true;
				}
				if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
				if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
				return 0;
			});

			if(callback instanceof Function){
				callback();
			}
		});
	}

	// This is where the data object is initialized, after which the render() function is called
	function init(){
		for(var index = C.DATASPAN[1]; index >= C.DATASPAN[0]; index--){
			D.seasonList.push(index);
		}

		populateWinnerList(C.DATASPAN[1], function(){
			render();
			setTimeout(function(){
				T.animateList('.season-list li', true);
			}, 500)
			setTimeout(function(){
				T.animateList('.winner-list li', true);
			}, 700);
		});
	}

	// Renders the current state of the data object. Optionally accepts two/three (with CB) arguments which are combined to "refresh" a certain DOM element per its data counterpart.
	function render(props){
		// props = {dataNode, target, callback}

		var $app = T.qs(),
			seasonHtml = [],
			listHtml = [],
			winnerItem = function(item){
				return `<li class="${item.worldChampion ? 'champion' : ''}">
					<div>
						<h3>${item.name}</h3>
						<div class="details">
							<div><label>Date of Birth:</label> ${item.dob.split('-').reverse().join('/')}</div>
							<div><label>Nationality:</label> ${item.nationality}</div>
						</div>
					</div>
					<div><a href="${item.url}" target="_blank">Biography</a></div>
				</li>`;
			};

		if(props && props.dataNode && props.target){
			var $target = T.qs(props.target);

			props.dataNode.map(function(item, index){
				listHtml.push(winnerItem(item));
			});
			$target.innerHTML = listHtml.join('');
		} else {
			D.seasonList.map(function(item, index){
				seasonHtml.push(`<li class="${index === 0 ? 'selected' : ''}" data-season="${item}">${item}</li>`);
			});

			D.winnerList[C.DATASPAN[1]].map(function(item, index){
				listHtml.push(winnerItem(item));
			})
		}

		if(!$app.classList.contains('on')){
			$app.innerHTML = `<header>
				<img src="resource/icon/formula1_logo.svg" />
				<h1>The Beast List</h1>
			</header>
			<div class="container">
				<div>
					<h2>Season</h2>
					<ul class="season-list">${seasonHtml.join('')}</ul>
				</div>
				<div>
					<h2>Beasts</h2>
					<ul class="winner-list">${listHtml.join('')}</ul>
				</div>
			</div>`;

			setTimeout(function(){
				var $seasonList = T.qs('.season-list');
				$seasonList.addEventListener('click', handleSeasonList);
				$app.classList.add('on');
			}, 100);
		}
	}

	init();
})(window, document);
